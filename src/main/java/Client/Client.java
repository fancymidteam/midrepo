package Client;

import shared.Message;
import shared.Request;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.HashMap;


public class Client implements Serializable{
    private Socket socket;
    private int id;

    public Client(Socket socket){
        this.socket = socket;
    }
    public Client (int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    /**
     * sending map to the server
     * @param map HashMap for Client-Request pairs
     */
    public void startParticipating(HashMap<Client, Request> map){
        try {
            Thread.sleep(Main.SLEEP_TIME);
            socket.setSoTimeout(Main.TIMEOUT);
            sendMessage(map);
            System.out.println("Sent request to server.");
        }catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    /**
     * using an ObjectOutputStream to write the message into the stream
     * @param message hashmap with Cliens and their Requests
     */
    public void sendMessage(HashMap message){
        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(message);
            System.out.println("Sent " + message + " to server.");
        } catch (IOException e) {
            finish();
            e.printStackTrace();
        }
    }

    /**
     * closing the socket on Client-side
     */
    public void finish(){
        System.out.println("closing " + toString() + ".");
        try {
            socket.close();
        } catch (IOException e) {}
    }

    /**
     * using an InputStream to read a message from the Server
     * @return received message from the server
     * @throws SocketTimeoutException
     */
    private Message receive() throws SocketTimeoutException{
        ObjectInputStream ois;
        Message o = null;
        try {
            ois = new ObjectInputStream(socket.getInputStream());
            o = (Message) ois.readObject();
        }catch(SocketTimeoutException e){
            throw e;
        }catch (IOException | ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("Received " + o + " from the Server as a Response.");
        return o;
    }
}