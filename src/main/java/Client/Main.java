package Client;

import shared.Input;
import shared.Request;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;

public class Main {

        public static String COORDINATOR_IP = "localhost";
        public static int COORDINATOR_PORT = 5476;
        public static int TIMEOUT = 5000;
        public static int SLEEP_TIME = 0;

        private static String inputFileName;

    /**
     * starting the client: parsing the request file, establishing a connection to the server and invoke sending the requests to the server
     * @param args the input file with all of the client requests
     */
        public static void main(String[] args) {
            try {
                System.out.println(args[0]);
                inputFileName=args[0];
                Input input = new Input(inputFileName);
                HashMap<Client, Request> filledMap = input.parseFileaAndStore(input.getFileName());
                Socket socket = new Socket(COORDINATOR_IP, COORDINATOR_PORT);
                Client client = new Client(socket);
                client.startParticipating(filledMap);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
