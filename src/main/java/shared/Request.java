package shared;


/**
 * Created by Theresa on 11/29/2017.
 */
public class Request extends Message implements Comparable<Request> {

    private String RequestType; //418 oracle, tellMeNow, or countprimes
    private int RequestArgument; //specified in input file
    private int clientId;  //the client's id; also specified in the input file;
    private String importancy; // A for tellmenow und B for the others

    public Request (int clientId, String functionType, int parameter, String importancy){
        this.clientId= clientId;
        this.RequestType=functionType;
        this.RequestArgument=parameter;
        this.importancy = importancy;
    }

    public String getRequestType() {
        return RequestType;
    }

    public int getRequestCost() {
        return RequestArgument;
    }

    public int getClientId() { return clientId; }

    public String getImportancy() { return importancy; }

    @Override
    public int compareTo(Request req) {
        return importancy.compareTo(req.getImportancy());
    }
}