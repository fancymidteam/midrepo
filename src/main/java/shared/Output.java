package shared;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * Class generating output file with statistics times
 */
public class Output {

    String filename = "Statistics.txt";
    Writer writer = null;

    /**
     * Generating output file "Statistic.txt" in current directory
     * @param outputs is anarray containing statistic times of all 10 workers in order from 0 to 10
      */
    public void saveOutput(long[] outputs){
       try {
           String current = new java.io.File( "." ).getCanonicalPath();
           writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(current + "\\" +filename), StandardCharsets.UTF_8));
           System.out.println(current+"\\"+filename);
           int i = 0;
           int j = 0;
           while (i < outputs.length && j < 10) {
                writer.write("Worker: " + j +"\n");
                writer.write("Lifespan: " + outputs[i] + " nano seconds\n");
                writer.write("Time of processing: " + outputs[i+1] + " nano seconds\n");
                writer.write("Time of waiting: " + outputs[i+2] + " nano seconds\n\n");
                i += 3;
                j++;
           }
           writer.write("Average time that function tellMeNow needs to be executed: " + outputs[30] + " nano seconds\n");
           writer.write("Average time that function countPrimes needs to be executed: " + outputs[31] + " nano seconds\n");
           writer.write("Average time that function 418Oracle needs to be executed: " + outputs[32] + " nano seconds\n\n");
        } catch (IOException ex) {
            System.out.println("I didn't succeed to save statistics");
        } finally {
            try {writer.close();} catch (Exception ex) {/*ignore*/}
        }
    }

}
