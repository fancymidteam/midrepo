package shared;

import Server.Worker;

import java.util.ArrayList;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * Created by Theresa on 11/29/2017.
 */
public class Loadbalancer {

    private Request request;
    private int threshold = 10;
    private int failure_counter = 0; // counts how many times we did not succeed to assign a task to a worker because of overload
    private PriorityBlockingQueue<Request> priorityBlockingQueue;

    public Loadbalancer (PriorityBlockingQueue prioritisedQueue){
        this.priorityBlockingQueue=prioritisedQueue;
    }

    /**
     * is asking the Worker node if its queue is capable to take another task according to the threshold
     * @param worker the worker thread/node
     * @return true if worker can take one more task, false otherwise
     */
    public boolean pollNode (Worker worker){
        return (worker.getRequests().size() <= threshold);
    }

    /**
     * if worker is not overloaded: assigning the most important task of priorityBlockingQueue to the workers queue and reset failure counter
     * if worker is overloaded: increase failure counter
     * @param worker the worker that will receive a new task to process (in his queue)
     */
    public void assignTask(Worker worker){
        try {
            if (pollNode(worker)) {
                Request req = priorityBlockingQueue.take();
                synchronized (worker.getRequests()) {
                    worker.getRequests().add(req);
                    failure_counter=0;
                }
            } else {
                System.out.println("Worker: " + worker.getId() + " is overloaded.");
                failure_counter++;
                System.out.println("failure counter: " +failure_counter);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * sender initiated scheduling
     * if workers have been overloaded for too long (more than 10 unsuccessful tries to assign a task) Master is sleeping for 50 milli seconds
     * if workers have not been overloaded for so far:
     * asking a random worker to do a task; triggers assignTask
     * @param workers List of all active Worker Threads
     */
    public void distributeTasks (ArrayList<Worker> workers){
        if(failure_counter>10) {
            try {
                System.out.println("Master is sleeping for 50 millis.");
                TimeUnit.MILLISECONDS.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        int randomNum = ThreadLocalRandom.current().nextInt(0, 9 + 1);
        assignTask(workers.get(randomNum));
    }
}
