package shared;

/**
 * Created by Theresa on 11/30/2017.
 */
public class Response extends Message {
    String responseText;

    public Response (String text){
        this.responseText=text;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }
}
