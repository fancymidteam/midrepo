package shared;

import Client.Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Theresa on 11/29/2017.
 */
public class Input {

    private HashMap<Client, Request> CombiClientRequest = new HashMap<>();
    private String fileName;

    public Input (String fileName){
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    /**
     * parsing the input file; generating request for each line/input function.
     * generating clients for read client ids
     * @param fileName Path of the input file
     * @return HashMap with pairs of Clients and their Requests.
     */
    public HashMap parseFileaAndStore (String fileName){
        List<String> list;
        try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {
            //br returns as stream and convert it into a List
            list = br.lines().collect(Collectors.toList());
            //access lines in list and split into request arguments
            for (int i=0; i<list.size(); i++){
                String line = list.get(i);
                String[] simpleLine = line.split(",");
                int clientId = Integer.parseInt(simpleLine[0]);
                String importancy = "B";
                if (simpleLine[1].equals("tellmenow")){
                    importancy = "A";
                }
                //generate request according to function, parameter and clientId
                Request request = new Request(clientId, simpleLine[1], Integer.parseInt(simpleLine[2]), importancy);
                //generate client according to clientId
                Client client = new Client(clientId);
                CombiClientRequest.put(client, request);
            }
            System.out.println("successfully leaving parseFileAndStore.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return CombiClientRequest;
    }
}
