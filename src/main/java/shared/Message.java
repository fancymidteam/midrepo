package shared;

import java.io.Serializable;
/**
 * Created by Theresa on 11/30/2017.
 */
public class Message implements Serializable {
    public String type;

    public Message(){
    }

    public boolean equals(Object other){
        if(other instanceof Message){
            return (((Message) other).type.equals(type));
        }else{
            return false;
        }
    }

}
