package Server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Map;


public class ListenThread implements Runnable{
	private boolean stop;
	private int port;
	private ServerSocket socket;
	
	Map<Socket, Master> connections;
	
	public ListenThread(int port, Map<Socket, Master> connections){
		stop = false;
		this.port = port;
		this.connections = connections;
	}
	@Override
	public void run() {
		try {
			System.out.println("In ListenThread run method");
			socket = new ServerSocket(port);
			System.out.println("listening on: " + port);
			socket.setSoTimeout(500);
			
			while(!stop){
				Socket newClient = null;
				try{
					newClient = socket.accept();
					System.out.println("Accepted connection!");

					InetAddress ip = newClient.getInetAddress();
					int port = newClient.getPort();
					System.out.println("Got TCP connection with " + ip.toString() + ":" + port);
					
					Master master = new Master(newClient, connections);
					connections.put(newClient, master);
					System.out.println("connected");
				}catch(SocketTimeoutException e){
				}catch(IOException e){
					System.out.println("Could not accept client connection");
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			socket.close();
		} catch (IOException e) {}
	}
	
	public void finish(){
		System.out.println("stopping tcp server");
		stop = true;
	}
}
