package Server;

import shared.*;
import Client.*;

import java.io.*;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by Theresa on 11/29/2017.
 */
public class Master {

    private boolean stop;
    private boolean active;
    public Output output = new Output();

    private PriorityBlockingQueue<Request> priorityBlockingQueue = new PriorityBlockingQueue<>();

    private InputStream in;
    private OutputStream out;

    private Socket socket;
    private Map<Socket, Master> connections;

    private ArrayList<Worker> workerArray = new ArrayList<>();

    double[] resultAverage = {0,0,0};


    public Master(Socket socket, Map<Socket, Master> connections) {
        stop = false;
        this.socket = socket;
        this.connections = connections;

        try {
            socket.setSoTimeout(500);
            in = socket.getInputStream();
            out = socket.getOutputStream();
            active = true;
        } catch (IOException e) {
            e.printStackTrace();
            active = false;
            return;
        }
        ListenThread lt = new ListenThread();
        new Thread(lt).start();
    }

    /**
     * closing socket on the Server-side
     * trigger output generating
     */
    public void finish(){
        System.out.println("closing " + toString() + ".");
        stop = true;
        active = false;
        try {
            connections.remove(socket);
            socket.close();
            computeStatistics(Worker.tellMeNowTimes, 0);
            computeStatistics(Worker.countPrimesTimes, 1);
            computeStatistics(Worker.oracleTimes, 2);
            long[] outputs = new long[33];
            int i = 0;
            int j = 0;
            while(i < 3*10 && j < 10){
                outputs[i] = workerArray.get(j).times[0];
                outputs[i+1] = workerArray.get(j).times[1];
                outputs[i+2] = workerArray.get(j).times[2];
                i = i+3;
                j++;
            }
            outputs[30] = (long)resultAverage[0];
            outputs[31] = (long)resultAverage[1];
            outputs[32] = (long)resultAverage[2];
            output.saveOutput(outputs);
        } catch (IOException e) {}
    }

    /**
     * sending message via writing in ObjectOutputStream
     * @param message to send to the client
     */
    public void sendMessage(Message message){
        try {
            ObjectOutputStream oos = new ObjectOutputStream(out);
            oos.writeObject(message);
            System.out.println("Sent " + message.type);
        } catch (IOException e) {
            finish();
            e.printStackTrace();
        }
    }
    /**
     * iterate over hashmap and puts requests into a priorityBlockingQueue
     * @param o HashMap including Client-Request information
     */
    private PriorityBlockingQueue handleMessage(HashMap o){
        System.out.println("Received " + o);
        Iterator it = o.entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry)it.next();
            Request req = (Request) pair.getValue();
            priorityBlockingQueue.offer(req);
            it.remove(); // avoids a ConcurrentModificationException
        }
        System.out.println("PrioritiyBlockingQueue: " + priorityBlockingQueue);
        return priorityBlockingQueue;
    }

    private class ListenThread implements Runnable{
        /**
         * Master is reading the message from the inputstream
         * triggering handleMessage and startFixedThreads
         */
        @Override
        public void run() {
            while(!stop){
                try {
                    System.out.println("In Master run method");
                    ObjectInputStream ois = new ObjectInputStream(in);
                    HashMap <Client, Request> o = (HashMap) ois.readObject();
                    ois.close();
                    PriorityBlockingQueue queue = handleMessage(o);
                    startFixedThreads(queue);
                } catch(SocketTimeoutException e){
                    continue;
                }catch (ClassNotFoundException | IOException e) {
                    finish();
                }
            }
        }
    }

    public String toString(){
        return socket.getInetAddress().toString() + ":" + socket.getPort();
    }

    /**
     * starting fixed amount of threads for workers: 10
     * storing workers in an ArrayList for easier accessing
     * giving each Worker-thread an id (and an own queue for requests)
     * triggering Loadbalancing: as long as there are Requests to process, dirstribute tasks to workers
     * afterwards: shutting down all the workers
     * @param queue generated priorityBlockingQueue with Requests in most prioritised order
     */
    public void startFixedThreads(PriorityBlockingQueue queue){
        ExecutorService executor = Executors.newFixedThreadPool(10);// 10 Threads
        for (int i = 0; i < 10; i++) { // call the (Worker(i).run) 10 times
            Worker worker = new Worker(i);
            workerArray.add(worker);
            executor.submit(worker);
            handleNewWorkerOnline(worker, i);
        }
        Loadbalancer lb = new Loadbalancer(queue);
        while (!queue.isEmpty()){
            lb.distributeTasks(workerArray);
        }

        for (int i = 0; i < 10; i++){ //not sure yet if it works..
            Worker workerToShutdown = workerArray.get(i);
            workerToShutdown.stopWorkerThread();
        }
        executor.shutdown();
        try {
            executor.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException ignored) {
        }
    }
    /**
     * initial loadbalancing after threads were started
     * @param worker that was just started
     * @param id worker's id
     */
    public void handleNewWorkerOnline(Worker worker, int id) {
        if(!priorityBlockingQueue.isEmpty()){
            Loadbalancer lb = new Loadbalancer(priorityBlockingQueue);
            lb.assignTask(worker);
        }
    }

    public void computeStatistics(ArrayList<Long> list, int j){
        long average = 0;
        for (int i = 0; i < list.size(); i++){
            average = average + list.get(i);
        }
        resultAverage[j] = (double)(average/(list.size()));
    }
}
