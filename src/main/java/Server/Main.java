package Server;

import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;




public class Main {

	public static int LISTEN_PORT = 5476;

	/**
	 * starting the Server
	 * @param args not used in our case
     */
	public static void main(String[] args) {
		Map<Socket, Master> connections = new ConcurrentHashMap<>();

		ListenThread lt = new ListenThread(LISTEN_PORT, connections);
		new Thread(lt).start();
	}

}


