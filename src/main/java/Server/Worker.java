package Server;

import shared.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Theresa on 11/29/2017.
 */
public class Worker implements Runnable{

    private int id;

    private Queue<Request> requests;
    private long lifeSpan = 0; // time that a Threads exists (since method run is called)
    private long waitingTime = 0; // time for which thread was waiting for a task
    private long timeOfProcessing = 0; //only time of processing of requests without waiting etc.
    private long startTime;
    long[] times = new long [3];
    static ArrayList<Long> tellMeNowTimes = new ArrayList<>();
    static ArrayList<Long> oracleTimes = new ArrayList<>();
    static ArrayList<Long> countPrimesTimes = new ArrayList<>();

    private AtomicBoolean run = new AtomicBoolean(true);

    /**
     * generating Worker Thread with id, request-queue
     * @param id worker id
     */
    public Worker (int id){
        this.id=id;
        this.requests = new LinkedList<>();
    }

    /**
     * Generating empty Response and triggering method executeWork.
     * @param req incoming request that needs to be processed.
     */
    public void workerHandleRequest(Request req) {
        Response res = new Response("Initial empty Response. Must be changed/overwritten.");
        executeWork(req, res);
    }

    /**
     * Triggering execution of the 3 different functions itself.
     * @param req The client's request that needs to be processed.
     * @param resp The generated response that will be sent to the client.
     */
    private void executeWork(Request req, Response resp) {
        switch (req.getRequestType()){
            case "418Oracle":
                resp.setResponseText(oracle(req.getClientId()));
                break;
            case "tellmenow":
                resp.setResponseText(tellmenow(req.getClientId()));
                break;
            case "countPrimes":
                resp.setResponseText(countPrimes(req.getRequestCost(), req.getClientId()));
                break;
            default: System.out.println("Invalid function as Input.");
                break;
        }
        System.out.println("Work executed. " + resp.getResponseText());
    }

    /**
     * function that needs to be done immediately: not more than 150 ms of latency.
     * @param clientID information about the client.
     * @return simple String return value.
     */
    private String tellmenow (int clientID){
        long start = System.nanoTime();
        System.out.println("Doing function tellmenow. For client: "+clientID + ". I am Worker: " + this.getId());
        try {
            Thread.currentThread().sleep(5);
            System.out.println("I slept 50 milis");
        }catch(InterruptedException e){
            Thread.currentThread().interrupt(); // set interrupt flag
            System.out.println("Failed to execute tellmenow");
        }
        long executingTime = (System.nanoTime() - start);
        tellMeNowTimes.add(executingTime);
        return "result: tellmenow executed";
    }

    /**
     * function that always takes the same amount of time. CPU intensive.
     * @param clientID information about the client.
     * @return simple String return value.
     */
    private String oracle (int clientID){
        long start = System.nanoTime();
        System.out.println("Doing function 418Oracle. For client: "+clientID + ". I am Worker: " + this.getId());
        try {
            Thread.currentThread().sleep(200);
            System.out.println("I slept 2000milis");
        }catch(InterruptedException e){
            Thread.currentThread().interrupt(); // set interrupt flag
            System.out.println("Failed to execute 418Oracle");
        }
        long executingTime = (System.nanoTime() - start);
        oracleTimes.add(executingTime);
        return "result: 418Oracle executed";
    }

    /**
     * function that calculates all the prime numbers up to the input parameter.
     * time intensive for high arguments; cheap for small input.
     * @param number argument for calculating the prime number.
     * @param clientID information about the client.
     * @return simple String return value.
     */
    private String countPrimes (int number, int clientID) {
        long start = System.nanoTime();
        System.out.println("Doing function countPrimes. With parameter: " + number + ". For Client: " + clientID + ". I am Worker: " + this.getId());
        try {
            int time = number*10;
            Thread.currentThread().sleep(time);
            System.out.println("Worker " + this.id + " slept "+ time+" milis");
        }catch(InterruptedException e){
            Thread.currentThread().interrupt(); // set interrupt flag
            System.out.println("Failed to execute countPrimes");
        }
        long executingTime = (System.nanoTime()) - start;
        countPrimesTimes.add(executingTime);
        return "result: countPrimes executed";
    }

    /**
     * Worker Thread is waiting until he has tasks in his queue to process
     */
    @Override
    public void run() {
        System.out.println("Starting Worker thread: " + id);
        startTime = System.nanoTime();
        boolean ran = false;
        while (run.get()) {
                try {
                    waitForRunnable();
                    ran = executeRunnable();
                    
                } catch (Throwable e) { //Throwable exceptionInRunnable??
                } finally {
                    if (ran) {
                        ran = false;
                    }
                }
        }
        System.out.println("Worker " + id + " is shutdown.");
        System.out.println("Time of processing: " + timeOfProcessing +"nano seconds.");
        System.out.println("Time of waiting: " + waitingTime +"nano seconds.");
        System.out.println("I was on for: " + getLifeSpan(System.nanoTime()) +"nano seconds.");
        times[1] = timeOfProcessing;
        times[2] = waitingTime;
        times[0] = getLifeSpan(System.nanoTime());
    }

    public int getId() {
        return id;
    }

    public Queue<Request> getRequests() {
        return requests;
    }

    /**
     * worker polls first element of his queue to process
     * and triggers workerHandleRequest
     * @return true if worker executes task successfully, false otherwise
     */
    private boolean executeRunnable() {
        long startTime=System.nanoTime();
        Request request;
        synchronized (requests) {
            request = requests.poll();
        }
        if (request != null)
            {System.out.println("Worker " + id + " got a new request to run from the queue");
            workerHandleRequest(request); //check if master is putting elements into beginning or end of workers queue
           timeOfProcessing=timeOfProcessing+(System.nanoTime()-startTime);
            return true;
        } else{
            return false;}

    }

    /**
     * waiting for Master to put Requests into workers queue
     * giving lock free on requests-queue
     */
    private void waitForRunnable() {
        System.out.println("Got the lock on my queue. Check whether there are tasks in my queue. " + "Worker: " + id);
        long startTime=System.nanoTime();
        synchronized (requests){
            while ((requests.isEmpty()) && run.get()) {
                System.out.println("No task, wait for 500ms and then check again");
                try {
                    requests.wait(500);
                } catch (InterruptedException e) {
                    System.out.println("Task thread was interrupted." + e);
                }
            }
        }
        waitingTime = waitingTime+(System.nanoTime()-startTime); // how long thread was waiting for a task
    }

    public void stopWorkerThread() {
        synchronized (requests) {
            run.set(false);
        }
    }

    public long getLifeSpan(long endTime){
        lifeSpan = endTime - startTime;
        return lifeSpan;
    }
}
