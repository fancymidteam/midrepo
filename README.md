#How to run the program:
- input argument: file path to file with client's requests
- start the method main in Server.Main
- start the method main in Client.Main
- wait/see the processing and statistical output
- terminate the server
- for further information: javaDoc can be created

Work division:
#Theresa Kölnberger:
- initial Server structure
- parsing Input
- priorityQueue, general data structures and other basic class setup
- threading basics
- scheduling
- javaDoc
- debugging

#Stephanie Bulela:
- initial Client structure
- scheduling
- costs of functions, making threads sleep for a certain amount of time/cost
- checking how adding and removing into queues is implemented (e.g. if master is putting elements into beginning or end of workers queue)
- statistics, counter for thread life time, capacities, etc.
- generating output file with statistics
- debugging

#Ideas for further implementations (if the project would go on)
- work with multiple sockets to make it more realistic
- parse the input more dynamically and prioritise dynamically
- therefore make priorityBlockingQueues for Workers
- send Responses back to the Clients (therefore Master needs to handle the responses)
- maybe work with extra loadbalancing thread? Or 2 Master Threads (one for initiate loadbalancing, one for retrieving responses from Workers)
- dynamic size of threads, killing threads if necessary, improve scheduling
- ...
------------------------------------------------------

#When to use what:

start working:
//update the whole project
//make sure u are at the master branch for that:
1. git checkout master
2. git pull

//switch to ur own branch for developing and update ur branch with the changings from the master branch:
3. git checkout steph
4. git rebase master

//develop ur java code

//when u wanna commit and push some code do following:
5. git status -- for an overview what files are modified
6. git add src/main/java/editedFile.java -- u can do that step several times for different files that u may have edited
7. git commit -m "some message" --will commit all the files that u added in the step before

//u can go on developing now or push ur stuff to the server:
8. git push
9. //u can do a pull request now: either copy the link that is shown or do it in bitbucket manually

------------------------------------------------------
1. "git pull" -- updating/downloading actual state -> getting latest information from server
2. "git push" -- uploading ur stuff to the server -> means updating the remote server with ur changes

3. "git status" -- use it a lot :)

4. "git add src/main/java/blabla.java"

5. "git commit -m "some describing message""

6. "git branch" -- see all the existing branches and which branch u r at right now

-----------------
-----------------
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: abc
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).